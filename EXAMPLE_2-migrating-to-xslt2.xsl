<?xml version="1.0" encoding="UTF-8"?>

<!--
    Copyright (C) 2015 ----------------.  All rights reserved.
    Authored by ---------------------------------.

    Title:			EXAMPLE_2-migrating-to-xslt2.xsl
	In/out:			flow 50: ( portal flat XML "execution" -> XML "Execution" ) -> SAP ORDERS05                                                           
    Description:	------------ Portal Project (https://ws.onehub.com/workspaces/-).
    Notes:          Expects parameters.
-->

<xsl:stylesheet version="1.5" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xalan/java" exclude-result-prefixes="java">

    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>

    <xsl:strip-space elements="*"/>

    <!-- 
       Global variables and parameters 
    -->
    <!-- From Adapter - value: test or live -->
    <xsl:param name="ENV" select="'test'"/>
    <!-- new line -->
    <xsl:variable name="newLine">
        <xsl:text>&#xa;</xsl:text>
    </xsl:variable>
    <!-- tab -->
    <xsl:variable name="tabChar">
        <xsl:text>&#x9;</xsl:text>
    </xsl:variable>
    <!-- lookup -->
    <xsl:variable name="lookup" select="document('lookup_lu0000001002.xml')"/>

    <!-- current date as yyyymmdd -->
    <xsl:variable name="sCurrDate">
        <xsl:sequence select="format-date( current-date(), '[Y][M00][D00]' )">
            <xsl:fallback> 
                <!-- time zone offset in ms that is to be applied -->
                <xsl:variable name="nTZOffset" select="3600000"/>
                <!-- Java Date object holding current date time at UK server -->
                <xsl:variable name="oDate" select="java:java.util.Date.new()" />
                <!-- current UTC date in "Unix" miliseconds plus time zone offset -->
                <xsl:variable name="nDateUnix" select="java:getTime( $oDate )  + $nTZOffset " />
                <xsl:variable name="oDateAdjusted" select="java:java.util.Date.new( $nDateUnix )" />
                <!-- current UK date and time in format yyyyMMdd HH:mm:ss -->
                <xsl:variable name="oSimpleDateFormat" select="java:java.text.SimpleDateFormat.new('yyyyMMdd HH:mm:ss')" />
                <xsl:variable name="sDateTime" select="java:format( $oSimpleDateFormat, $oDateAdjusted )" />
                <!-- current date: yyyymmdd -->
                <xsl:variable name="sCurrDate" select="substring-before($sDateTime, ' ')" />
                <!-- current time: hh:mm:ss -->
                <xsl:value-of select="substring-before($sDateTime, ' ')" /> 
            </xsl:fallback>
        </xsl:sequence>
    </xsl:variable>

    <!-- current time as hh:mm:ss -->
    <xsl:variable name="sCurrTime">
        <xsl:sequence select="format-time( current-time(), '[H]:[m00]:[s00]' )">
            <xsl:fallback> 
                <!-- time zone offset in ms that is to be applied -->
                <xsl:variable name="nTZOffset" select="3600000"/>
                <!-- Java Date object holding current date time at UK server -->
                <xsl:variable name="oDate" select="java:java.util.Date.new()" />
                <!-- current UTC date in "Unix" miliseconds plus time zone offset -->
                <xsl:variable name="nDateUnix" select="java:getTime( $oDate )  + $nTZOffset " />
                <xsl:variable name="oDateAdjusted" select="java:java.util.Date.new( $nDateUnix )" />
                <!-- current UK date and time in format yyyyMMdd HH:mm:ss -->
                <xsl:variable name="oSimpleDateFormat" select="java:java.text.SimpleDateFormat.new('yyyyMMdd HH:mm:ss')" />
                <xsl:variable name="sDateTime" select="java:format( $oSimpleDateFormat, $oDateAdjusted )" />
                <!-- current date: yyyymmdd -->
                <xsl:variable name="sCurrDate" select="substring-before($sDateTime, ' ')" />
                <!-- current time: hh:mm:ss -->
                <xsl:value-of select="substring-after($sDateTime, ' ')" /> 
            </xsl:fallback>
        </xsl:sequence>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="DVGExecution" name="newEnvelope">

        <Envelope>
            <!-- xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="file:///D:/adaptris/schema/v5/execution.xsd"> -->
            <EnvelopeHeader>
                <SchemaVersion>5</SchemaVersion>
                <EnvelopeCreated>
                    <xsl:value-of select="concat( $sCurrDate, ' ', $sCurrTime )"/>
                </EnvelopeCreated>
                <EnvelopeTrackingID>
                    <xsl:value-of select="Order/order_id"/>
                </EnvelopeTrackingID>
                <EnvelopeRevisionNumber>1</EnvelopeRevisionNumber>
                <SourcePartnerID>lu0000001002</SourcePartnerID>
                <SourceDivisionID>lu0000001002</SourceDivisionID>
                <DestinationPartnerID></DestinationPartnerID>
                <DestinationDivisionID></DestinationDivisionID>
                <TestIndicator>
                    <xsl:choose>
                        <xsl:when test="$ENV='test'">True</xsl:when>
                        <xsl:otherwise>False</xsl:otherwise>
                    </xsl:choose>
                </TestIndicator>
            </EnvelopeHeader>

            <xsl:apply-templates/>

            <EnvelopeTrailer>
                <TotalMessageCount>1</TotalMessageCount>
            </EnvelopeTrailer>
        </Envelope>

    </xsl:template>

    <xsl:template match="Order" name="newExecutionDocument">
        <ExecutionDocument DocumentSequenceNumber="1" ExecutionDocumentType="Confirmation Of Acceptance Without Change" MessageLifecycle="New">
            <DocumentHeader Language="DE">
                <xsl:attribute name="Currency">
                    <xsl:value-of select="currency"/>
                </xsl:attribute>

                <DocumentReference AssignedBy="Buyer" Type="Purchase Order Number">
                    <xsl:value-of select="order_id"/>
                </DocumentReference>

                <DocumentReference AssignedBy="F4F" Type="Order Request Number">
                    <xsl:value-of select="f4f_id"/>
                </DocumentReference>

                <DocumentDate Type="Document Created" AssignedBy="Buyer">
                    <xsl:value-of select="$sCurrDate"/>
                </DocumentDate>
                <DocumentDate Type="Purchase Order Date" AssignedBy="Buyer">
                    <xsl:value-of select="order_date"/>
                </DocumentDate>

                <Organisation Type="Buyer">
                    <Reference AssignedBy="Supplier" Type="Organisation ID">
                        <xsl:value-of select="account_id"/>
                    </Reference>
                </Organisation>
                <Organisation Type="Supplier">
                    <Reference Type="Division ID" AssignedBy="Supplier">
                        <xsl:value-of select="org_id"/>
                    </Reference>
                </Organisation>

                <MovementDetails DeliveryCollectionIndicator="Delivery" Phase="1" Movement="1">
                    <xsl:if test="delivery_wish_date !=''">
                        <!--<Date Type="Expected Arrival Date" AssignedBy="Supplier">
                            <!-\- Date in ms since Unix big bang -\->
                            <xsl:variable name="tmpDateFromAPI" select="concat( delivery_wish_date, '000' ) + 0"/>
                            <!-\- Date object holding current date from API -\->
                            <xsl:variable name="oDate" select="java:java.util.Date.new( $tmpDateFromAPI )"/>
                            <!-\- SimpleDateFormat object -\->
                            <xsl:variable name="oFormatYYYYMMDD" select="java:java.text.SimpleDateFormat.new( 'yyyyMMdd' )"/>
                            <!-\- apply format -\->
                            <xsl:value-of select="java:format( $oFormatYYYYMMDD, $oDate )"/>
                            </Date>-->
                        <Date Type="Expected Arrival Date" AssignedBy="Supplier">
                            <xsl:value-of select="delivery_wish_date"/>
                        </Date>
                    </xsl:if>
                    <Location Type="Delivery Point">
                        <Reference AssignedBy="Supplier" Type="Location Code">
                            <xsl:value-of select="org_id"/>
                        </Reference>
                        <Reference AssignedBy="Supplier" Type="Organisation ID">
                            <xsl:value-of select="shipto_id"/>
                        </Reference>
                    </Location>
                </MovementDetails>
                
				<!-- comments for order -->
				<xsl:if test="comments != ''">
					<Narrative AssignedBy="Buyer" Type="Special Instructions">
						<xsl:value-of select="comments" />
					</Narrative>
				</xsl:if>

            </DocumentHeader>

            <!-- DocumentLines -->
            <xsl:apply-templates select="OrderLines/OrderLine" mode="newDocumentLine"/>

            <DocumentTrailer>
                <Totals TotalType="Control">
                    <TotalLineCount>
                        <xsl:value-of select="count(../OrderLine)"/>
                    </TotalLineCount>
                </Totals>
            </DocumentTrailer>

        </ExecutionDocument>
    </xsl:template>

    <!-- DocumentLine(s) -->
    <xsl:template match="OrderLine" mode="newDocumentLine">
        <DocumentLine>
            <xsl:attribute name="LineNumber">
                <xsl:value-of select="line_number"/>
            </xsl:attribute>
            <xsl:attribute name="LineStatus">
                <xsl:value-of select="'New'"/>
            </xsl:attribute>

            <LineReference AssignedBy="Buyer" Type="Contract Number">
                <xsl:attribute name="LineNumber">
                    <xsl:value-of select="contract_line"/>
                </xsl:attribute>
                <xsl:value-of select="contract_id"/>
            </LineReference>

            <LineDate Type="Purchase Order Date" AssignedBy="Supplier">
                <xsl:value-of select="order_date"/>
            </LineDate>

            <MovementDetails DeliveryCollectionIndicator="Delivery" Phase="1" Movement="1">
               <Location Type="Silo">
                    <Reference AssignedBy="Buyer" Type="Location Code">
                        <xsl:value-of select="silo_id"/>
                    </Reference>
                </Location>
            </MovementDetails>

            <LineComponent>
                <Product>
                    <Specification>
                        <Reference AssignedBy="Supplier" Type="Identifier">
                            <xsl:value-of select="product_id"/>
                        </Reference>
                        <Reference AssignedBy="Supplier" Type="Group Code">
                            <xsl:value-of select="animal_species"/>
                        </Reference>
                    </Specification>
                    <Quantity Type="Confirmed" AssignedBy="Supplier">
                        <xsl:attribute name="UnitOfMeasure">
                            <xsl:call-template name="lookup">
                                <xsl:with-param name="description" select="'UnitOfMeasurement'"/>
                                <xsl:with-param name="code" select="quantity_ordered_uom"/>
                            </xsl:call-template>
                        </xsl:attribute>
                        <xsl:value-of select="quantity_ordered"/>
                    </Quantity>
                </Product>
            </LineComponent>

        </DocumentLine>
    </xsl:template>
    

    <!-- 
		utility templates 
	-->

    <!--
        Name: 		lookup  
        Parameters: code - value to be replaced with line value from line where @code is equal to code 
        description - attribute from Lookup element like below
        Output:		replacing value
        <line code="KG" description="UnitOfMeasurement">Kilogramme</line>
    -->
    <xsl:template name="lookup">
        <xsl:param name="code"/>
        <xsl:param name="description"/>
        <xsl:variable name="lookedup" select="$lookup/Lookup/line[@code = $code][@description=$description]"/>
        <xsl:choose>
            <xsl:when test="$lookedup != ''">
                <xsl:value-of select="$lookedup"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$code"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- catch all do nothing template, just to move over everything -->
    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>

</xsl:stylesheet>
