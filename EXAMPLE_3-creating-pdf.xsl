<?xml version="1.0" encoding="UTF-8"?>

<!--
    Copyright (C) 2015 ----------------.  All rights reserved.
    Authored by ---------------------------------.

    Title:			EXAMPLE_3-creating-pdf.xsl
	In/out:			SAP ORDRSP -> ( XML "Execution" -> XSL FO PDF Aufbereitungsmitteilung )
    Description:	-----------
    Notes:          Expects parameters.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:java="http://xml.apache.org/xalan/java" exclude-result-prefixes="java" >

    <!--  <xsl:output method="xml" encoding="UTF-8"/>  -->
    
	<xsl:decimal-format name="DE-de" grouping-separator="." decimal-separator="," />

    <xsl:template match="/">
    
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

            <fo:layout-master-set>
                <fo:simple-page-master margin-right="13mm" margin-left="13mm" margin-bottom="5mm" margin-top="5mm" page-width="297mm" page-height="210mm" master-name="page">
                    <fo:region-body margin-bottom="15mm" margin-top="15mm"/>
                    <fo:region-before extent="20mm"/>
                    <fo:region-after extent="10mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="page">

                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="right" font-size="6pt"> Seite <fo:page-number/>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="/Envelope/ExecutionDocument"/>
                </fo:flow>
            
            </fo:page-sequence>

        </fo:root>

    </xsl:template>
  

    <xsl:template match="ExecutionDocument">

        <fo:block font-weight="bold" font-size="16pt" space-after="10mm" text-align="left">Aufbereitungsmitteilung</fo:block>
    
        <fo:block font-size="10pt" space-after="6mm">
            <fo:table width="33%" border="2pt solid black" border-collapse="collapse">
                <fo:table-column column-width="35%" />
                <fo:table-column />
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block font-weight="bold">Aufbereiter:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block>------ GmbH, Dünsen</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block font-weight="bold">Datum:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block>
                                <xsl:value-of select="substring(DocumentHeader/DocumentDate[@Type='Purchase Order Date'], 7, 2)"/>
                                <xsl:text>.</xsl:text>
                                <xsl:value-of select="substring(DocumentHeader/DocumentDate[@Type='Purchase Order Date'], 5, 2)"/>
                                <xsl:text>.</xsl:text>
                                <xsl:value-of select="substring(DocumentHeader/DocumentDate[@Type='Purchase Order Date'], 1, 4)"/>                 
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block font-weight="bold">Bestellnummer:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block>
                                <xsl:value-of select="DocumentHeader/DocumentReference[@AssignedBy='Buyer'][@Type='Purchase Order Number']"/>                        
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block font-weight="bold">Bestellposition:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block>
                                <xsl:value-of select="DocumentLine/LineReference/@LineNumber"/>
                            </fo:block>
                        </fo:table-cell>                            
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block font-weight="bold">Endlieferung:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" border="0.5pt dotted grey" display-align="center" >
                            <fo:block>
                                <xsl:choose>
                                    <xsl:when test="DocumentHeader/DocumentReference[@AssignedBy='Supplier'][@Type='Purchase Order Number']/@SubType='END'">
                                        <xsl:text>X</xsl:text>
                                    </xsl:when>
                                </xsl:choose>                        
                            </fo:block>
                        </fo:table-cell>                            
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    
        <xsl:comment>main table for storing 2 tables: lines table and "Chargenklassifikation" table</xsl:comment>
        <fo:table border-style="none">
        
            <!-- width of left table -->
            <fo:table-column column-width="216mm"/>
            <fo:table-column />
        
            <fo:table-body>
                <fo:table-row>

                    <xsl:comment>table for positions: product and components</xsl:comment>
                    <fo:table-cell>
                        <fo:block font-size="10pt">
                            <fo:table border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="black" border-after-width="2pt" border-after-style="solid" border-start-color="black" border-start-width="2pt" border-start-style="solid" border-end-style="none" border-collapse="collapse">
            
                                <xsl:comment>Materialnr</xsl:comment>
                                <fo:table-column column-width="18mm" />

                                <xsl:comment>Materialkurztext</xsl:comment>
                                <fo:table-column column-width="60mm" />

                                <xsl:comment>SAP-Charge</xsl:comment>
                                <fo:table-column column-width="21mm" />
                                
                                <xsl:comment>Anerkennungsnr</xsl:comment>
                                <fo:table-column column-width="34.5mm" />

                                <xsl:comment>kg-brutto</xsl:comment>
                                <fo:table-column column-width="20.5mm" />

                                <xsl:comment>kg-netto</xsl:comment>
                                <fo:table-column column-width="20.5mm" />

                                <xsl:comment>Anzahl</xsl:comment>                                
                                <fo:table-column column-width="20.5mm" />

                                <xsl:comment>EH</xsl:comment>                                
                                <fo:table-column column-width="9mm" />

                                <xsl:comment>kg pro Eh</xsl:comment>
                                <fo:table-column column-width="12mm" />
                                
                                <xsl:comment>lines table header</xsl:comment>
                                <fo:table-header font-weight="bold" height="24pt">
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">Material-</fo:block>
                                            <fo:block text-align="center">nummer</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">Material-</fo:block>
                                            <fo:block text-align="center">kurztext:</fo:block>
                                        </fo:table-cell> 
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">SAP-</fo:block>
                                            <fo:block text-align="center">Charge</fo:block>                                                                
                                        </fo:table-cell> 
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">Anerkennungs-</fo:block>
                                            <fo:block text-align="center">nummer</fo:block>                                
                                        </fo:table-cell> 
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">kg</fo:block>
                                            <fo:block text-align="center">brutto</fo:block>                                
                                        </fo:table-cell> 
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">kg</fo:block>
                                            <fo:block text-align="center">netto</fo:block>                                
                                        </fo:table-cell> 
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">Anzahl /</fo:block>
                                            <fo:block text-align="center">Menge</fo:block>                                
                                        </fo:table-cell> 
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">EH</fo:block>
                                        </fo:table-cell> 
                                        <fo:table-cell display-align="center" padding="1pt" border="2pt solid black">  
                                            <fo:block text-align="center">kg/EH</fo:block>
                                            <fo:block text-align="center">bfn</fo:block>                                
                                        </fo:table-cell>                            
                                    </fo:table-row>
                                </fo:table-header>
                                
                                <fo:table-body>
                                
                                    <fo:table-row>
                                       <fo:table-cell display-align="center" padding="1pt" number-columns-spanned="2" border-style="none">
                                            <fo:block text-align="left" font-weight="bold">Hergestellte Menge:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                                            <fo:block text-align="center"/> 
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>                                                                                       
                                    </fo:table-row>
        
                                    <xsl:comment>primary line(s)</xsl:comment>
                                    <xsl:apply-templates select="DocumentLine/LineComponent/Product[@Type='Primary']" mode="primary"/>                  
                                    <xsl:comment>/primary line(s)</xsl:comment>
                                    
                                    <xsl:comment>komponenten saatgut header</xsl:comment>
                                    <fo:table-row>
                                       <fo:table-cell display-align="center" padding="1pt" number-columns-spanned="2" border-before-color="black" border-before-style="solid" border-before-width="2pt">
                                            <fo:block text-align="left" font-weight="bold">Komponenten Saatgut:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/> 
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">                                                
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">                                                
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">                                            
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>                                                                                       
                                    </fo:table-row>                                                               
        
                                    <xsl:comment>komponenten saatgut lines</xsl:comment>
                                    <xsl:apply-templates select="DocumentLine/LineComponent/Product[@Type='Component'][starts-with(Specification/Reference[@Type='Identifier'][@AssignedBy='Supplier'], '2')]" mode="component"/>
        
                                    <xsl:comment>komponenten rhb header</xsl:comment>
                                    <fo:table-row>
                                       <fo:table-cell display-align="center" padding="1pt" number-columns-spanned="2" border-before-color="black" border-before-style="solid" border-before-width="2pt">
                                            <fo:block text-align="left" font-weight="bold">Komponenten RHB:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/> 
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">                                                
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">                                                
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border-before-color="black" border-before-style="solid" border-before-width="2pt" border-after-color="grey" border-after-style="dotted" border-after-width="0.5pt" border-start-color="grey" border-start-width="0.5pt" border-start-style="dotted">                                            
                                            <fo:block text-align="center"/>
                                        </fo:table-cell>                                                                                       
                                    </fo:table-row>                                                                             
        
                                    <xsl:comment>komponenten RHB line(s)</xsl:comment>
                                    <xsl:apply-templates select="DocumentLine/LineComponent/Product[@Type='Component'][starts-with(Specification/Reference[@Type='Identifier'][@AssignedBy='Supplier'], '3')]" mode="componentrhb"/>  
        
                                </fo:table-body>
                            </fo:table>
                        </fo:block>                            
                    </fo:table-cell>
                    <!-- /table for lines -->
        
                    <xsl:comment>table for Chargenklassifikation data</xsl:comment>
                    <fo:table-cell>
                        <fo:block font-size="10pt">
                            <fo:table border="2pt solid black" border-collapse="collapse">
        
                                <fo:table-column column-width="35mm" />
                                <fo:table-column />
                            
                                <fo:table-header font-weight="bold">
                                    <fo:table-row height="24pt">
                                        <fo:table-cell number-columns-spanned="2" display-align="center" padding="1pt" border="2pt solid black">
                                            <fo:block text-align="center">Chargenklassifikation:</fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </fo:table-header>
                                
                                <fo:table-body>

                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Unit/Palette:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:if test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='UNITSPERPALETTE']/NumericResult!=''">
                                                    <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='UNITSPERPALETTE']/NumericResult"/>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>

                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Kaliber:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='KALIBER']/NumericResult"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>

                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>TKG:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='TKG']/NumericResult,'.',',')"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>

                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Plombierungsdatum:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:call-template name="format-date">
                                                    <xsl:with-param name="date" select="DocumentLine/LineDate[@AssignedBy='Supplier'][@Type='Sample Taken Date']" />
                                                </xsl:call-template>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Reinheit:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='REINHEIT']/NumericResult,'.',',')" />     
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Keimfähigkeit:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='KEIMFAEHIGKEIT']/NumericResult,'.',',')"/>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>KF anomale:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='KF_ANORMALE']/NumericResult!=''">
                                                        <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='KF_ANORMALE']/NumericResult,'.',',')"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>0</xsl:otherwise>
                                                </xsl:choose>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>KF tote:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='KF_TOTE']/NumericResult !=''">
                                                        <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='KF_TOTE']/NumericResult,'.',',')"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>X</xsl:otherwise>
                                                </xsl:choose>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Kalttest:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='KALTTEST']/NumericResult,'.',',')"/>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>CT anomale:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='CT_ANORMALE']/NumericResult!=''">
                                                        <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='CT_ANORMALE']/NumericResult,'.',',')"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>0</xsl:otherwise>
                                                </xsl:choose>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>CT tote:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='CT_TOTE']/NumericResult!=''">
                                                        <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='CT_TOTE']/NumericResult,'.',',')"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>0</xsl:otherwise>
                                                </xsl:choose>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Feuchtigkeit:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='FEUCHTIGKEIT']/NumericResult,'.',',')"/>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Analysenummer:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/Criteria/CriteriaID"/>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Analysedatum:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:call-template name="format-date">
                                                    <xsl:with-param name="date" select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisDate" />
                                                </xsl:call-template>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Untersuchungsstelle:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/Criteria/CriteriaDescription"/>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Heubachwert:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HEUBACH']/NumericResult!=''">
                                                        <xsl:value-of select="translate(DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HEUBACH']/NumericResult,'-','')"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>unbek.</xsl:otherwise>
                                                </xsl:choose>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Aufwuchsland 1:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_1']/NumericResult!=''">
                                                        <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_1']/NumericResult"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>unbek.</xsl:otherwise>
                                                </xsl:choose>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Aufwuchsland 2:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_2']/NumericResult!=''">
                                                        <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_2']/NumericResult"/>
                                                    </xsl:when>
                                                </xsl:choose>                                            
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Aufwuchsland 3:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_3']/NumericResult!=''">
                                                        <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_3']/NumericResult"/>
                                                    </xsl:when>
                                                </xsl:choose>                                                                                        
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Aufwuchsland 4:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_4']/NumericResult!=''">
                                                        <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_4']/NumericResult"/>
                                                    </xsl:when>
                                                </xsl:choose>                                                                                        
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                    <fo:table-row>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>Aufwuchsland 5:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey" >
                                            <fo:block>
                                                <xsl:choose>
                                                    <xsl:when test="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_5']/NumericResult!=''">
                                                        <xsl:value-of select="DocumentLine/LineComponent/Product/Specification/Quality/AnalysisResult[@AnalysisBasis='HERKUNFTSLAND_5']/NumericResult"/>
                                                    </xsl:when>
                                                </xsl:choose>                                                                                        
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    
                                </fo:table-body>
                            </fo:table>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
        
    </xsl:template>


    <!-- process primary lines eg that are NOT "Komponenten" -->
    <xsl:template match="Product" mode="primary">
    
        <xsl:comment>primary line</xsl:comment>
        
        <fo:table-row>
        
            <xsl:comment>Materialnr</xsl:comment>        
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:value-of select="Specification/Reference[@Type='Identifier'][@AssignedBy='Supplier']"/>            
                </fo:block>
            </fo:table-cell>
    
            <xsl:comment>Materialkurztext</xsl:comment>        
            <fo:table-cell display-align="center" padding="1pt" padding-start="2pt" padding-end="2pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:value-of select="Specification/Description[@Type='Short'][@AssignedBy='Supplier']"/>
                </fo:block>
            </fo:table-cell>
    
            <xsl:comment>SAP-Charge</xsl:comment>        
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:value-of select="Specification/Reference[@Type='Lot'][@AssignedBy='Buyer']"/>            
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>Anerkennungsnr</xsl:comment>        
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <xsl:call-template name="split">
                    <xsl:with-param name="string" select="Specification/Reference[@Type='Batch'][@AssignedBy='Buyer']" />
                    <xsl:with-param name="length" select="17" />
                </xsl:call-template>
            </fo:table-cell>
            
            <xsl:comment>kg brutto</xsl:comment>        
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end">
                    <xsl:if test="format-number( Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTBRUTTO']/NumericResult, '#.##0,000', 'DE-de' ) != 'NaN'">
                        <xsl:value-of select="format-number (Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTBRUTTO']/NumericResult, '#.##0,00', 'DE-de')"/>
                    </xsl:if>            
                </fo:block>
            </fo:table-cell>
    
            <xsl:comment>kg netto</xsl:comment>        
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end">
                    <xsl:if test="format-number( Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTNETTO']/NumericResult, '#.##0,000', 'DE-de' ) != 'NaN'">
                        <xsl:value-of select="format-number( Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTNETTO']/NumericResult,'#.##0,00', 'DE-de')"/>
                    </xsl:if>            
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>Anzahl</xsl:comment>        
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end">
                    <xsl:if test="format-number( Quantity, '#.##0,000', 'DE-de') != 'NaN'">
                        <xsl:value-of select="format-number( Quantity, '#.##0,00', 'DE-de' )"/>
                    </xsl:if>
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>EH</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:call-template name="uom">
                        <xsl:with-param name="uom" select="Quantity[@Type='Ordered']/@UnitOfMeasure"/>
                    </xsl:call-template>            
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>kg/EH</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:if test="format-number( Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTPROEINHEIT_BRUTTO']/NumericResult, '#.###,00', 'DE-de') != 'NaN'">
                        <xsl:value-of select="format-number(Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTPROEINHEIT_BRUTTO']/NumericResult, '#.###,00', 'DE-de')"/>
                    </xsl:if>                                
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    
    </xsl:template>


    <!-- processes lines for "Komponenten Saatgut" in PDF. The only difference to "Komponenten RHB" is, that Batch and Lot numbers ARE created in output. -->
    <xsl:template match="Product" mode="component">
    
        <xsl:comment>component line</xsl:comment>
        
        <fo:table-row>
        
            <xsl:comment>Materialnr</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:value-of select="Specification/Reference[@Type='Identifier'][@AssignedBy='Supplier']"/>
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>Materialtext</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-start="2pt" padding-end="2pt" border="0.5pt dotted grey">
                <fo:block text-align="center">            
                    <xsl:value-of select="Specification/Description[@Type='Short'][@AssignedBy='Supplier']" />
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>SAP - Charge</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:value-of select="Specification/Reference[@Type='Lot'][@AssignedBy='Buyer']"/>
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>Anerkennungsnr</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <xsl:call-template name="split">
                    <xsl:with-param name="string" select="Specification/Reference[@Type='Batch'][@AssignedBy='Buyer']" />
                    <xsl:with-param name="length" select="17" />
                </xsl:call-template>
            </fo:table-cell>
            
            <xsl:comment>kg-brutto</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end"/>
            </fo:table-cell>
            
            <xsl:comment>kg-netto</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end">
                    <xsl:if test="format-number( Quantity[@Type='Ordered'][@AssignedBy='Buyer'][@UnitOfMeasure='Kilogramme'],'#.##0,000' ,'DE-de') != 'NaN'">
                        <xsl:value-of select="format-number( Quantity[@Type='Ordered'][@AssignedBy='Buyer'][@UnitOfMeasure='Kilogramme'],'#.##0,00' ,'DE-de')"/>
                    </xsl:if>            
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>Anzahl</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end">
                    <xsl:if test="format-number( Quantity, '#.##0,000', 'DE-de' ) != 'NaN'">
                        <xsl:value-of select="format-number( Quantity, '#.##0,00', 'DE-de' )"/>
                    </xsl:if>
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>Einheit</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:call-template name="uom">
                        <xsl:with-param name="uom" select="Quantity[@Type='Ordered']/@UnitOfMeasure"/>
                    </xsl:call-template>            
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>kg per Einheit</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:if test="format-number(Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTPROEINHEIT_BRUTTO']/NumericResult, '#.##0,00', 'DE-de') != 'NaN'">
                        <xsl:value-of select="format-number(Specification/Quality/AnalysisResult[@AnalysisBasis='GEWICHTPROEINHEIT_BRUTTO']/NumericResult, '#.##0,00', 'DE-de')"/>            
                    </xsl:if>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
        
    </xsl:template>


    <!-- process lines for "Komponenten RHB". The only difference to "Komponenten Saatgut" is, that no Batch and Lot numbers are created in output. -->
    <xsl:template match="DocumentLine/LineComponent/Product" mode="componentrhb">
    
        <xsl:comment>component RHB line</xsl:comment>
    
        <fo:table-row>
    
            <xsl:comment>Materialnr</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:value-of select="Specification/Reference[@Type='Identifier'][@AssignedBy='Supplier']"/>            
                </fo:block>
            </fo:table-cell>
    
            <xsl:comment>Materialtext</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-start="2pt" padding-end="2pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:value-of select="Specification/Description[@Type='Short'][@AssignedBy='Supplier']"/>
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>SAP Charge</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center"/>
            </fo:table-cell>
            
            <xsl:comment>Anerk.-Nr</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center"/>
            </fo:table-cell>
    
            <xsl:comment>kg-brutto</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="center"/>
            </fo:table-cell>
            
            <xsl:comment>kg-netto</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end">
                    <xsl:if test="format-number ( Quantity[@Type='Ordered'][@AssignedBy='Buyer'][@UnitOfMeasure='Kilogramme'], '#.##0,000', 'DE-de') != 'NaN'">
                        <xsl:value-of select="format-number ( Quantity[@Type='Ordered'][@AssignedBy='Buyer'][@UnitOfMeasure='Kilogramme'], '#.##0,00', 'DE-de')"/>
                    </xsl:if>            
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>Anzahl</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" padding-end="3pt" border="0.5pt dotted grey">
                <fo:block text-align="end">
                    <xsl:if test="format-number( Quantity, '#.##0,000', 'DE-de' ) != 'NaN' ">
                        <xsl:value-of select="format-number( Quantity, '#.##0,00', 'DE-de' )"/>
                    </xsl:if>            
                </fo:block>
            </fo:table-cell>
    
            <xsl:comment>Einheit</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center">
                    <xsl:choose>
                        <xsl:when test="Quantity[@Type='Ordered']/@UnitOfMeasure='Unit'">STK</xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="uom">
                                <xsl:with-param name="uom" select="Quantity[@Type='Ordered']/@UnitOfMeasure"/>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                </fo:block>
            </fo:table-cell>
            
            <xsl:comment>kg pro Einh.</xsl:comment>
            <fo:table-cell display-align="center" padding="1pt" border="0.5pt dotted grey">
                <fo:block text-align="center"/>
            </fo:table-cell>
    
        </fo:table-row>
    
    </xsl:template>

    
    <xsl:template name="uom">
        <xsl:param name="uom"/>
        <xsl:choose>
            <xsl:when test="$uom='Kilogramme'">KG</xsl:when>
            <xsl:when test="$uom='Unit'">UN</xsl:when>
            <xsl:when test="$uom='PCE'">STK</xsl:when>
            <xsl:when test="$uom='Litre'">L</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring( $uom, 1, 3) "/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>    

	<!-- 
        Name:   format-date 
        Params:
            inputFormat     - format of input text date in java.text.SimpleDateFormat syntax
            date            - string with date to be reformatted
            outputFormat    - format of input text date in java.text.SimpleDateFormat syntax
        Output: date string formatted according to outputFormat
    -->
    <xsl:template name="format-date">
        <xsl:param name="inputFormat" select="'yyyyMMdd'" />
        <xsl:param name="date" />
        <xsl:param name="outputFormat" select="'dd.MM.yyyy'" />
        
        <xsl:choose>
            <xsl:when test="string( $date ) ">
                <xsl:variable name="javaInputFormat" select="java:java.text.SimpleDateFormat.new( $inputFormat )" />
                <xsl:variable name="javaOutputFormat" select="java:java.text.SimpleDateFormat.new( $outputFormat )" />
                <xsl:variable name="javaDate" select="java:parse( $javaInputFormat, $date )" />
                <xsl:value-of select="java:format( $javaOutputFormat, $javaDate )" />            
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>

	<!-- 
        Name:   split
        Params:
            string          - string to split
            length          - # of chars
        Output: split a string into of blocks with text of defined length
    -->
    <xsl:template name="split">
        <xsl:param name="string" select="''" />
        <xsl:param name="length" select="string-length( $string )" />

        <xsl:choose>
            <xsl:when test="string-length( $string ) > $length ">
                <fo:block text-align="center" keep-together.within-line="always">
                    <xsl:value-of select="substring( $string, 1, $length ) "/>
                </fo:block> 
                <xsl:call-template name="split">
                    <xsl:with-param name="string" select="substring( $string, $length + 1 )" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <fo:block text-align="center" keep-together.within-line="always">
                    <xsl:value-of select="$string" />
                </fo:block> 
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


</xsl:stylesheet>
