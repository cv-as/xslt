<?xml version="1.0" encoding="UTF-8"?>

<!--
    Copyright (C) ---------------------.  All rights reserved.
    Authored by -----------------------.

    Title:			EXAMPLE_1-creating-SAP-IDoc-ORDERS.xsl
	In/out:			flow 50: portal flat XML " execution" -> ( XML "Execution"  -> SAP ORDERS05 )
    Description:	- Portal Project (https://ws.onehub.com/workspaces/-).
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xalan/java" exclude-result-prefixes="java">

    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>

    <xsl:strip-space elements="*"/>

    <!-- 
       Global variables and parameters 
    -->
    <!-- From Adapter - value: test or live -->
    <xsl:param name="ENV" select="'test'"/>
    <!-- lookup -->
    <xsl:variable name="lookup" select="document('Lookup_---.xml')"/>
    <!-- new line -->
    <xsl:variable name="newLine">
        <xsl:text>&#xa;</xsl:text>
    </xsl:variable>
    <!-- tab -->
    <xsl:variable name="tabChar">
        <xsl:text>&#x9;</xsl:text>
    </xsl:variable>

    <!-- current date as yyyymmdd -->
    <xsl:variable name="sCurrDate">
        <!-- time zone offset in ms that is to be applied -->
        <xsl:variable name="nTZOffset" select="0"/>
        <!-- Java Date object holding current date time at UK server -->
        <xsl:variable name="oDate" select="java:java.util.Date.new()"/>
        <!-- current UTC date in "Unix" miliseconds plus time zone offset -->
        <xsl:variable name="nDateUnix" select="java:getTime( $oDate )  + $nTZOffset "/>
        <xsl:variable name="oDateAdjusted" select="java:java.util.Date.new( $nDateUnix )"/>
        <!-- current UK date and time in format yyyyMMdd HH:mm:ss -->
        <xsl:variable name="oSimpleDateFormat" select="java:java.text.SimpleDateFormat.new('yyyyMMdd HH:mm:ss')"/>
        <xsl:variable name="sDateTime" select="java:format( $oSimpleDateFormat, $oDateAdjusted )"/>
        <!-- current date: yyyymmdd -->
        <xsl:variable name="sCurrDate" select="substring-before($sDateTime, ' ')"/>
        <!-- current time: hh:mm:ss -->
        <xsl:value-of select="substring-before($sDateTime, ' ')"/>
    </xsl:variable>

    <!-- current time as hh:mm:ss -->
    <xsl:variable name="sCurrTime">
        <!-- time zone offset in ms that is to be applied -->
        <xsl:variable name="nTZOffset" select="0"/>
        <!-- Java Date object holding current date time at UK server -->
        <xsl:variable name="oDate" select="java:java.util.Date.new()"/>
        <!-- current UTC date in "Unix" miliseconds plus time zone offset -->
        <xsl:variable name="nDateUnix" select="java:getTime( $oDate )  + $nTZOffset "/>
        <xsl:variable name="oDateAdjusted" select="java:java.util.Date.new( $nDateUnix )"/>
        <!-- current UK date and time in format yyyyMMdd HH:mm:ss -->
        <xsl:variable name="oSimpleDateFormat" select="java:java.text.SimpleDateFormat.new('yyyyMMdd HHmmss')"/>
        <xsl:variable name="sDateTime" select="java:format( $oSimpleDateFormat, $oDateAdjusted )"/>
        <!-- current date: yyyymmdd -->
        <xsl:variable name="sCurrDate" select="substring-before($sDateTime, ' ')"/>
        <!-- current time: hh:mm:ss -->
        <xsl:value-of select="substring-after($sDateTime, ' ')"/>
    </xsl:variable>

    <!-- random int -->
    <xsl:variable name="sLongRandom1">
        <xsl:variable name="oRandomGenerator" select="java:java.util.Random.new()"/>
        <xsl:variable name="nSignedLongRandom" select="java:nextInt ( $oRandomGenerator )"/>
        <!-- abs() -->
        <xsl:value-of select=" $nSignedLongRandom * ( boolean ( $nSignedLongRandom>0 ) - not( boolean ( $nSignedLongRandom>0>0)) )"/>
    </xsl:variable>

    <xsl:variable name="sLongRandom2">
        <xsl:variable name="oRandomGenerator" select="java:java.util.Random.new()"/>
        <xsl:variable name="nSignedLongRandom" select="java:nextInt ( $oRandomGenerator )"/>
        <!-- abs() -->
        <xsl:value-of select=" $nSignedLongRandom * ( boolean ( $nSignedLongRandom>0 ) - not( boolean ( $nSignedLongRandom>0>0)) )"/>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="Envelope" name="newORDERS05">
        <ORDERS05>
            <IDOC BEGIN="1">
                <xsl:apply-templates/>
            </IDOC>
        </ORDERS05>
    </xsl:template>

    <xsl:template match="EnvelopeHeader" name="newEDI_DC40">
        <EDI_DC40 SEGMENT="1">
            <TABNAM>EDI_DC40</TABNAM>
            <MANDT>100</MANDT>
            <DOCNUM>
                <xsl:value-of select="format-number( $sLongRandom1, '0000000000000000' )"/>
            </DOCNUM>
            <DOCREL>731</DOCREL>
            <STATUS>30</STATUS>
            <DIRECT>1</DIRECT>
            <OUTMOD>2</OUTMOD>
            <EXPRSS/>
            <TEST/>
            <IDOCTYP>ORDERS05</IDOCTYP>
            <CIMTYP/>
            <MESTYP>ORDERS</MESTYP>
            <MESCOD/>
            <MESFCT/>
            <STD/>
            <STDVRS/>
            <STDMES/>
            <SNDPOR>FIRST4FARM</SNDPOR>
            <SNDPRT>LS</SNDPRT>
            <SNDPFC/>
            <SNDPRN>F4F</SNDPRN>
            <SNDSAD/>
            <SNDLAD/>
            <RCVPOR>SAPDVE</RCVPOR>
            <RCVPRT>LS</RCVPRT>
            <RCVPFC>LS</RCVPFC>
            <RCVPRN>
                <xsl:choose>
                    <xsl:when test="TestIndicator='True'">DVEMAND100</xsl:when>
                    <xsl:otherwise>DVPCLNT100</xsl:otherwise>
                </xsl:choose>
            </RCVPRN>
            <RCVSAD/>
            <RCVLAD/>
            <CREDAT>
                <xsl:value-of select="$sCurrDate"/>
            </CREDAT>
            <CRETIM>
                <xsl:value-of select="$sCurrTime"/>
            </CRETIM>
            <REFINT/>
            <REFGRP/>
            <REFMES/>
            <ARCKEY/>
            <SERIAL>
                <xsl:value-of select="format-number( $sLongRandom2, '0000000000000000' )"/>
            </SERIAL>
        </EDI_DC40>
    </xsl:template>

    <xsl:template match="DocumentHeader" name="newAllbutLines">
        <E1EDK01 SEGMENT="1">
            <ACTION>000</ACTION>
            <KZABS/>
            <CURCY>
                <xsl:value-of select="@Currency"/>
            </CURCY>
            <HWAER/>
            <WKURS/>
            <ZTERM/>
            <KUNDEUINR/>
            <EIGENUINR/>
            <BSART/>
            <BELNR>
                <xsl:value-of select="DocumentReference[@Type='Purchase Order Number'][@AssignedBy='Buyer']"/>
            </BELNR>
            <NTGEW/>
            <BRGEW/>
            <GEWEI/>
            <FKART_RL/>
            <ABLAD/>
            <BSTZD/>
            <VSART/>
            <VSART_BEZ/>
            <RECIPNT_NO/>
            <KZAZU/>
            <AUTLF/>
            <AUGRU/>
            <AUGRU_BEZ/>
            <ABRVW/>
            <ABRVW_BEZ/>
            <FKTYP/>
            <LIFSK/>
            <LIFSK_BEZ/>
            <EMPST/>
            <ABTNR/>
            <DELCO/>
            <WKURS_M/>
        </E1EDK01>
        <!-- Verkaufsorganisation (orgid from Contract)-->
        <E1EDK14 SEGMENT="1">
            <QUALF>008</QUALF>
            <ORGID>
                <xsl:value-of select="Organisation[@Type='Supplier']/Reference[@Type='Division ID']"/>
            </ORGID>
        </E1EDK14>
        <!-- Auftragsart ZTA0=ORDER   -->
        <E1EDK14 SEGMENT="1">
            <QUALF>012</QUALF>
            <ORGID>ZTA0</ORGID>
        </E1EDK14>
        <!-- Vertriebsweg -->
        <E1EDK14 SEGMENT="1">
            <QUALF>007</QUALF>
            <ORGID>01</ORGID>
        </E1EDK14>
        <!-- Sparte   -->
        <E1EDK14 SEGMENT="1">
            <QUALF>006</QUALF>
            <ORGID>01</ORGID>
        </E1EDK14>
        <!-- Bestellart (SD)  -->
        <E1EDK14 SEGMENT="1">
            <QUALF>019</QUALF>
            <ORGID>WEBP</ORGID>
        </E1EDK14>
        <!-- Verkaufsbuero digits 1,2 are digits 1,2 from  Verkaufsorganisation/ digits 3,4 are always 99      -->
        <E1EDK14 SEGMENT="1">
            <QUALF>016</QUALF>
            <ORGID>
                <xsl:value-of select="substring(Organisation[@Type='Supplier']/Reference[@Type='Division ID'],1,2)"/>
                <xsl:text>99</xsl:text>
            </ORGID>
        </E1EDK14>
        <E1EDK03 SEGMENT="1">
            <IDDAT>012</IDDAT>
            <DATUM>
                <xsl:value-of select="DocumentDate[@Type='Document Created'][@AssignedBy='Buyer']"/>
            </DATUM>
            <UZEIT/>
        </E1EDK03>
        <E1EDK03 SEGMENT="1">
            <IDDAT>022</IDDAT>
            <DATUM>
                <xsl:value-of select="DocumentDate[@Type='Purchase Order Date'][@AssignedBy='Buyer']"/>
            </DATUM>
            <UZEIT/>
        </E1EDK03>
        <!-- Wunschlieferdatum Kunde-->
        <E1EDK03 SEGMENT="1">
            <IDDAT>002</IDDAT>
            <DATUM>
                <xsl:value-of select="MovementDetails/Date[@Type='Expected Arrival Date']"/>
            </DATUM>
            <UZEIT/>
        </E1EDK03>
        <E1EDKA1 SEGMENT="1">
            <PARVW>AG</PARVW>
            <PARTN>
                <xsl:value-of select="Organisation[@Type='Buyer']/Reference[@Type='Organisation ID']"/>
            </PARTN>
        </E1EDKA1>
        <E1EDKA1 SEGMENT="1">
            <PARVW>WE</PARVW>
            <PARTN>
                <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Reference[@Type='Organisation ID']"/>
            </PARTN>
        </E1EDKA1>
        <E1EDK02 SEGMENT="1">
            <QUALF>001</QUALF>
            <BELNR>
                <xsl:value-of select="DocumentReference[@Type='Purchase Order Number'][@AssignedBy='Buyer']"/>
            </BELNR>
            <DATUM>
                <xsl:value-of select="DocumentDate[@Type='Purchase Order Date'][@AssignedBy='Buyer']"/>
            </DATUM>
            <UZEIT/>
        </E1EDK02>
        <E1EDK02 SEGMENT="1">
            <QUALF>007</QUALF>
            <BELNR>
                <xsl:value-of select="DocumentReference[@Type='Order Request Number'][@AssignedBy='F4F']"/>
            </BELNR>
            <DATUM>
                <xsl:value-of select="DocumentDate[@Type='Purchase Order Date'][@AssignedBy='Buyer']"/>
            </DATUM>
            <UZEIT/>
        </E1EDK02>
        <E1EDK02 SEGMENT="1">
            <QUALF>043</QUALF>
            <BELNR>
                <xsl:value-of select="../DocumentLine/LineReference[@Type='Contract Number'][@AssignedBy='Buyer']"/>
            </BELNR>
        </E1EDK02>
    </xsl:template>

    <xsl:template match="DocumentLine" name="newE1EDP01">
        <E1EDP01 SEGMENT="1">
            <POSEX>
                <xsl:value-of select="@LineNumber"/>
            </POSEX>
            <ACTION>000</ACTION>
            <PSTYP/>
            <KZABS/>
            <MENGE>
                <xsl:value-of select="LineComponent/Product/Quantity[@Type='Confirmed'][@AssignedBy='Supplier']"/>
            </MENGE>
            <MENEE>
                <xsl:call-template name="lookupReverse">
                    <xsl:with-param name="description" select="'UnitOfMeasurement'"/>
                    <xsl:with-param name="code" select="LineComponent/Product/Quantity[@Type='Confirmed'][@AssignedBy='Supplier']/@UnitOfMeasure"/>
                </xsl:call-template>
            </MENEE>
            <BMNG2/>
            <PMENE/>
            <ABFTZ/>
            <VPREI/>
            <PEINH/>
            <NETWR/>
            <ANETW/>
            <SKFBP/>
            <NTGEW/>
            <GEWEI/>
            <EINKZ/>
            <CURCY>
                <xsl:value-of select="../DocumentHeader/@Currency"/>
            </CURCY>
            <PREIS/>
            <MATKL>
                <xsl:value-of select="LineComponent/Product/Specification/Reference[@Type='Group Code'][@AssignedBy='Supplier']"/>
            </MATKL>
            <UEPOS/>
            <GRKOR/>
            <EVERS/>
            <BPUMN/>
            <BPUMZ/>
            <ABGRU/>
            <ABGRT/>
            <ANTLF/>
            <FIXMG/>
            <KZAZU/>
            <BRGEW/>
            <PSTYV/>
            <EMPST/>
            <ABTNR/>
            <ABRVW/>
            <WERKS/>
            <LPRIO/>
            <LPRIO_BEZ/>
            <ROUTE/>
            <ROUTE_BEZ/>
            <LGORT/>
            <VSTEL/>
            <DELCO/>
            <MATNR/>
            <VALTG/>
            <HIPOS/>
            <HIEVW/>
            <POSGUID/>
            <MATNR_EXTERNAL/>
            <MATNR_VERSION/>
            <MATNR_GUID/>
            <IUID_RELEVANT/>
            <E1EDP03 SEGMENT="1">
                <IDDAT>022</IDDAT>
                <DATUM>
                    <xsl:value-of select="../DocumentHeader/DocumentDate[@Type='Purchase Order Date'][@AssignedBy='Buyer']"/>
                </DATUM>
                <UZEIT/>
            </E1EDP03>
            <E1EDP03 SEGMENT="1">
                <IDDAT>012</IDDAT>
                <DATUM>
                    <xsl:value-of select="../DocumentHeader/DocumentDate[@Type='Document Created'][@AssignedBy='Buyer']"/>
                </DATUM>
                <UZEIT/>
            </E1EDP03>

            <xsl:if test="LineReference[@Type='Contract Number'][@AssignedBy='Buyer']=''">
                <!-- Materialnummer des Lieferanten if Contract available-->
                <E1EDP19 SEGMENT="1">
                    <QUALF>002</QUALF>
                    <IDTNR>
                        <xsl:value-of select="LineComponent/Product/Specification/Reference[@Type='Identifier'][@AssignedBy='Supplier']"/>
                    </IDTNR>
                    <KTEXT>
                        <xsl:value-of select="LineComponent/Product/Specification/Description"/>
                    </KTEXT>
                    <MFRPN/>
                    <MFRNR/>
                    <IDTNR_EXTERNAL/>
                    <IDTNR_VERSION/>
                    <IDTNR_GUID/>
                </E1EDP19>
            </xsl:if>
            <!-- externes Lieferwerk -->
            <E1EDP02>
                <QUALF>083</QUALF>
                <BELNR>
                    <xsl:value-of select="'2250'"/>
                </BELNR>
            </E1EDP02>
            <E1EDP02 SEGMENT="1">
                <QUALF>043</QUALF>
                <BELNR>
                    <xsl:value-of select="LineReference[@Type='Contract Number'][@AssignedBy='Buyer']"/>
                </BELNR>
                <POSNR>
                    <xsl:value-of select="LineReference[@Type='Contract Number'][@AssignedBy='Buyer']/@LineNumber"/>
                </POSNR>
            </E1EDP02>
            <xsl:if test="LineReference[@Type='Contract Number'][@AssignedBy='Buyer']!=''">
                <!-- Materialnummer des Lieferanten if Contract NOT available-->
                <E1EDP02 SEGMENT="1">
                    <QUALF>044</QUALF>
                    <IHREZ>
                        <xsl:value-of select="LineComponent/Product/Specification/Reference[@Type='Identifier'][@AssignedBy='Supplier']"/>
                    </IHREZ>
                </E1EDP02>
            </xsl:if>
            <!-- Kundensilo -->
            <E1EDPT1 SEGMENT="1">
                <TDID>ZP07</TDID>
                <TSSPRAS>D</TSSPRAS>
                <TSSPRAS_ISO>DE</TSSPRAS_ISO>
                <E1EDPT2 SEGMENT="1">
                    <TDLINE>
                        <xsl:value-of select="MovementDetails/Location[@Type='Silo']/Reference[@Type='Location Code']"/>
                    </TDLINE>
                    <TDFORMAT/>
                </E1EDPT2>
            </E1EDPT1>

			<!-- order comment split to 70 chars -->
			<xsl:for-each select="../DocumentHeader/Narrative[ .!='' ]">
				<xsl:call-template name="newE1EDPT1Comment">
					<xsl:with-param name="sComment" select="." />
				</xsl:call-template>				
			</xsl:for-each>
            
        </E1EDP01>
    </xsl:template>

    <xsl:template match="DocumentTrailer" name="newE1EDS01">
        <E1EDS01 SEGMENT="1">
            <SUMID>001</SUMID>
            <SUMME>
                <xsl:value-of select="Totals[@TotalType='Control']/TotalLineCount"/>
            </SUMME>
            <SUNIT/>
            <WAERQ/>
        </E1EDS01>
    </xsl:template>

	<!-- text with order comment, split to 70 chars -->
	<xsl:template name="newE1EDPT1Comment">
		<xsl:param name="sComment" />

		<E1EDPT1 SEGMENT="1">
            <TDID>ZP01</TDID>
            <TSSPRAS>D</TSSPRAS>
            <TSSPRAS_ISO>DE</TSSPRAS_ISO>
            <E1EDPT2 SEGMENT="1">
                <TDLINE>
                    <xsl:value-of select="substring( $sComment, 1, 70 )"/>
                </TDLINE>
                <TDFORMAT/>
            </E1EDPT2>
        </E1EDPT1>

		<xsl:if test="string-length( $sComment ) > 70 ">
			<xsl:call-template name="newE1EDPT1Comment">
				<xsl:with-param name="sComment">
					<xsl:value-of select="substring( $sComment, 71 )" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>		
	</xsl:template>
	
    <!-- 
		utility templates 
	-->

    <!--
        Name: 		lookupReverse  
        Parameters: value - value that should be replaced with @code from lookup
                    description - attribute from Lookup element, like
        Output:		replacing value
        Comment:    <line code="KGM" description="UnitOfMeasurement">Kilogramme</line>
    -->
    <xsl:template name="lookupReverse">
        <xsl:param name="code"/>
        <xsl:param name="description"/>
        <xsl:variable name="lookedup" select="$lookup/Lookup/line[. = $code][@description=$description]/@code"/>
        <xsl:choose>
            <xsl:when test="$lookedup != ''">
                <xsl:value-of select="$lookedup"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$code"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- catch all do nothing template, just to move over everything -->
    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>

</xsl:stylesheet>
