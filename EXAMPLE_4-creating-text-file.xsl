<?xml version="1.0" encoding="UTF-8"?>

<!--
	Copyright (C) ---------------------.  All rights reserved.
	Authored by -------------------------.

	Title:			EXAMPLE_4-creating-text-file.xsl
	In/out:			SAP INVOIC01 -> ( XML "Financial" -> text file )
	Description:	---- invoices for ---.
	Notes:			expects paramter from adapter	
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xalan/java" exclude-result-prefixes="java">
	
	<xsl:output method="text" encoding="UTF-8" indent="no" omit-xml-declaration="yes" />
    
    <xsl:strip-space elements="*"/>
    
    <!-- 
       Global variables and parameters 
    -->
    <!-- filename for output text file -->
    <xsl:param name="ADPTR_MSGID" select="'b9bb2f41-27f4-4820-9794-6faab346c935.txt'" />
	<!-- new line -->
    <xsl:variable name="newLine"><xsl:text>&#xa;</xsl:text></xsl:variable>
	<!-- tab -->
	<xsl:variable name="tabChar"><xsl:text>&#x9;</xsl:text></xsl:variable>
        		
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="//Envelope/FinancialDocument/@FinancialDocumentType = 'Tax Invoice'"><xsl:text>InvoiceNumber;InvoiceUUID;InvoiceDate;InvoiceTaxPointDate;InvoiceCurrencyCode;InvoiceBuyersOrderID;InvoiceSellersOrderID;InvoiceOrderDate;InvoiceFileReferenceID;InvoiceContractReferenceID;InvoiceSenderCustomerAssignedAccountID;InvoiceSenderPartyID;InvoiceSenderPartyIDScheme;InvoiceSenderPartyName;InvoiceSenderStreetName;InvoiceSenderAdditionalStreetName;InvoiceSenderCityName;InvoiceSenderPostalZone;InvoiceSenderCountryCode;InvoiceReceiverSupplierAssignedAccountID;InvoiceReceiverPartyID;InvoiceReceiverPartyIDScheme;InvoiceReceiverPartyName;InvoiceReceiverStreetName;InvoiceReceiverAdditionalStreetName;InvoiceReceiverCityName;InvoiceReceiverPostalZone;InvoiceReceiverCountryCode;InvoiceDeliveryLocationID;InvoiceDeliveryLocationDescription;InvoiceDeliveryStreetName;InvoiceDeliveryAdditionalStreetName;InvoiceDeliveryCityName;InvoiceDeliveryPostalZone;InvoiceDeliveryCountryCode;InvoicePaymentMeansCode;InvoicePaymentTermsNote;InvoiceTotalTaxAmount;InvoiceSubTotalTaxableAmount;InvoiceSubTotalTaxAmount;InvoiceSubTotalTaxCategoryID;InvoiceSubTotalTaxCategoryPercent;InvoiceSubTotalTaxSchemeID;InvoiceTotalLineAmount;InvoiceTaxTotal;InvoiceTotal;InvoicePayableAmount;InvoiceLineID;InvoiceLineQuantity;InvoiceLineUnitCode;InvoiceLineExtensionAmount;InvoiceLineTotalTaxAmount;InvoiceLineSubTotalTaxableAmount;InvoiceLineSubTotalTaxAmount;InvoiceLineSubTotalTaxCategoryID;InvoiceLineSubTotalTaxCategoryPercent;InvoiceLineSubTotalTaxSchemeID;InvoiceLineItemDescription;InvoiceLineSellersItemID;InvoiceLineSellersItemIDScheme;InvoiceLineUnitPrice;InvoiceLineBaseQuantityUnitCode</xsl:text></xsl:when>
			<xsl:otherwise><xsl:text>CreditNoteNumber;CreditNoteUUID;CreditNoteDate;CreditNoteFileReferenceID;CreditNoteTaxPointDate;CreditNoteCurrencyCode;CreditNoteBuyersOrderID;CreditNoteSellersOrderID;CreditNoteOrderDate;CreditNoteInvoiceID;CreditNoteInvoiceDate;CreditNoteContractReferenceID;CreditNoteSenderCustomerAssignedAccountID;CreditNoteSenderPartyID;CreditNoteSenderPartyIDScheme;CreditNoteSenderPartyName;CreditNoteSenderStreetName;CreditNoteSenderAdditionalStreetName;CreditNoteSenderCityName;CreditNoteSenderPostalZone;CreditNoteSenderCountryCode;CreditNoteReceiverSupplierAssignedAccountID;CreditNoteReceiverPartyID;CreditNoteReceiverPartyIDScheme;CreditNoteReceiverPartyName;CreditNoteReceiverStreetName;CreditNoteReceiverAdditionalStreetName;CreditNoteReceiverCityName;CreditNoteReceiverPostalZone;CreditNoteReceiverCountryCode;CreditNoteTotalTaxAmount;CreditNoteSubTotalTaxableAmount;CreditNoteSubTotalTaxAmount;CreditNoteSubTotalTaxCategoryID;CreditNoteSubTotalTaxCategoryPercent;CreditNoteSubTotalTaxSchemeID;CreditNoteTotalLineAmount;CreditNoteTaxTotal;CreditNoteTotal;CreditNotePayableAmount;CreditNoteLineID;CreditNoteLineQuantity;CreditNoteLineUnitCode;CreditNoteLineExtensionAmount;CreditNoteLineTotalTaxAmount;CreditNoteLineSubTotalTaxableAmount;CreditNoteLineSubTotalTaxAmount;CreditNoteLineSubTotalTaxCategoryID;CreditNoteLineSubTotalTaxCategoryPercent;CreditNoteLineSubTotalTaxSchemeID;CreditNoteLineItemDescription;CreditNoteLineSellersItemID;CreditNoteLineSellersItemIDScheme;CreditNoteLineUnitPrice;CreditNoteLineBaseQuantityUnitCode</xsl:text></xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$newLine"/>		
        <xsl:apply-templates />
	</xsl:template>

	<xsl:template match="FinancialDocument[@FinancialDocumentType = 'Credit']/DocumentLine">
        <!-- CreditNoteNumber -->
        <xsl:value-of select="../DocumentHeader/DocumentReference[@AssignedBy='Supplier'][@Type='Invoice Number']"/><xsl:text>;</xsl:text>
        
        <!-- CreditNoteUUID -->
        <xsl:value-of select="../../EnvelopeHeader/EnvelopeTrackingID"/><xsl:text>;</xsl:text>
        
        <!-- CreditNoteDate -->
        <xsl:call-template name="formatDate">
			<xsl:with-param name="date"	select="../DocumentHeader/DocumentDate[@AssignedBy='Supplier'][@Type='Invoice Date']"/>
		</xsl:call-template>
		<xsl:text>;</xsl:text>

        <!-- CreditNoteFileReferenceID -->
        <xsl:value-of select="substring ( translate( $ADPTR_MSGID, '-', '' ), 1, 17 )" />
        <xsl:text>;</xsl:text>
		
		<!-- CreditNoteTaxPointDate --> 
        <xsl:call-template name="formatDate">
			<xsl:with-param name="date"	select="../DocumentHeader/DocumentDate[@AssignedBy='Supplier'][@Type='Tax Point Date']"/>
		</xsl:call-template>
		<xsl:text>;</xsl:text> 
		
		<!-- CreditNoteCurrencyCode -->
        <xsl:value-of select="../DocumentHeader/@Currency"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteBuyersOrderID -->
        <xsl:value-of select="../DocumentHeader/DocumentReference[@AssignedBy='Buyer'][@Type='Purchase Order Number']"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteSellersOrderID -->        
        <xsl:value-of select="../DocumentHeader/DocumentReference[@AssignedBy='Supplier'][@Type='Purchase Order Number']"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteOrderDate -->        
        <xsl:if test="../DocumentHeader/DocumentDate[@AssignedBy='Buyer'][@Type='Purchase Order Date'] != ''">
            <xsl:call-template name="formatDate">
                <xsl:with-param name="date"	select="../DocumentHeader/DocumentDate[@AssignedBy='Buyer'][@Type='Purchase Order Date']"/>
            </xsl:call-template>
		</xsl:if>
		<xsl:text>;</xsl:text>
		
		<!-- CreditNoteInvoiceID -->
        <xsl:value-of select="../DocumentHeader/DocumentReference[@AssignedBy='Supplier'][@Type='Invoice Number']"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteInvoiceDate -->
		<xsl:if test="../DocumentHeader/DocumentDate[@AssignedBy='Supplier'][@Type='Invoice Date'] != ''">
            <xsl:call-template name="formatDate">
                <xsl:with-param name="date"	select="../DocumentHeader/DocumentDate[@AssignedBy='Supplier'][@Type='Invoice Date']"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:text>;</xsl:text>           

        <!-- CreditNoteContractReferenceID -->
        <xsl:value-of select="../DocumentHeader/DocumentReference[@Type='Contract Number']"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteSenderCustomerAssignedAccountID -->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Reference[@Type='Organisation ID'][@AssignedBy='Third Party']"/>
        <xsl:text>;</xsl:text> 
        
        <!-- CreditNoteSenderPartyID -->       
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Reference[@Type='VAT Registration Number'][@AssignedBy='Government Agency']"/>
        <xsl:text>;</xsl:text>

        <!-- CreditNoteSenderPartyIDScheme -->
		<xsl:text>GB:VAT</xsl:text>
		<xsl:text>;</xsl:text>

		<!-- Sender info -->           
        <!-- CreditNoteSenderPartyName -->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/Name"/>
        <xsl:text>;</xsl:text>        
        
        <!-- CreditNoteSenderStreetName -->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/StreetAddress[1]"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteSenderAdditionalStreetName -->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/StreetAddress[2]"/>
        <xsl:text>;</xsl:text>  
        
        <!-- CreditNoteSenderCityName -->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/CityName"/>
        <xsl:text>;</xsl:text>                   
        
        <!--CreditNoteSenderPostalZone-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/PostalCode"/>
        <xsl:text>;</xsl:text>               
        
        <!-- CreditNoteSenderCountryCode -->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/CountryCode"/>
        <xsl:text>;</xsl:text>                

		<!-- Receiver info -->
        <!--CreditNoteReceiverSupplierAssignedAccountID-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Reference[@Type='Organisation ID'][@AssignedBy='Third Party']"/>
        <xsl:text>;</xsl:text>       
        
        <!--CreditNoteReceiverPartyID-->
        <xsl:call-template name="lookup">
            <xsl:with-param name="code">
                <xsl:call-template name="removeLeadingZeros">
                    <xsl:with-param name="removeLeadingZeros" select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Reference[@Type='Organisation ID'][@AssignedBy='Buyer']" />
                </xsl:call-template>
            </xsl:with-param>
            <xsl:with-param name="description" select="'-----------'" /> 
        </xsl:call-template>         
        <xsl:text>;</xsl:text>
        
        <!--CreditNoteReceiverPartyIDScheme-->
        <xsl:choose>
            <xsl:when test="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location/Address/CountryCode = 'IE'">
                <xsl:text>IE:VAT</xsl:text>
            </xsl:when>
            <xsl:otherwise>GB:VAT</xsl:otherwise>
        </xsl:choose>
		<xsl:text>;</xsl:text>
		
		<!--CreditNoteReceiverPartyName-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/Name"/>
        <xsl:text>;</xsl:text>
        
        <!--CreditNoteReceiverStreetName-->        
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/StreetAddress[1]"/>
        <xsl:text>;</xsl:text>
        
        <!--CreditNoteReceiverAdditionalStreetName-->            
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/StreetAddress[2]"/>
        <xsl:text>;</xsl:text>  
        
        <!--CreditNoteReceiverCityName-->                  
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/CityName"/>
        <xsl:text>;</xsl:text>                   
        
        <!--CreditNoteReceiverPostalZone-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/PostalCode"/>
        <xsl:text>;</xsl:text>               
        
        <!--CreditNoteReceiverCountryCode-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/CountryCode"/>
        <xsl:text>;</xsl:text>               

		<!-- Delivery Info -->	
        <!--xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Reference"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/Name"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/StreetAddress[1]"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/StreetAddress[2]"/>-<xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/StreetAddress[3]"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/CityName"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/PostalCode"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/CountryCode"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="../DocumentHeader/TermsAndConditions/PaymentTerms/PaymentMethod"/><xsl:text>;</xsl:text>        
        <xsl:value-of select="../DocumentHeader/TermsAndConditions/PaymentTerms/Reference"/><xsl:text>;</xsl:text-->        

		<!-- Invoice totals -->
		<!--CreditNoteTotalTaxAmount-->
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalTax"/>
        <xsl:text>;</xsl:text>
        
        <!--CreditNoteSubTotalTaxableAmount-->        
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueBeforeTax"/>
        <xsl:text>;</xsl:text>  
        
        <!--CreditNoteSubTotalTaxAmount-->      
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalTax"/>
        <xsl:text>;</xsl:text>   
        
        <!--CreditNoteSubTotalTaxCategoryID-->     
		<xsl:text>S</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!--CreditNoteSubTotalTaxCategoryPercent-->
        <xsl:value-of select="../DocumentTrailer/TaxTrailer/@TaxRate"/>
        <xsl:text>;</xsl:text>      
        
        <!--CreditNoteSubTotalTaxSchemeID-->  
		<xsl:text>VAT</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!--CreditNoteTotalLineAmount-->
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueBeforeTax"/>
        <xsl:text>;</xsl:text>
        
        <!--CreditNoteTaxTotal-->        
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalTax"/>
        <xsl:text>;</xsl:text> 
        
        <!--CreditNoteTotal-->       
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueAfterTax"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNotePayableAmount-->        
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueAfterTax"/>
        <xsl:text>;</xsl:text>   
        
        <!-- Documentline info -->
        <!-- CreditNoteLineID -->
        <xsl:value-of select="@LineNumber"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteLineQuantity -->        
        <xsl:value-of select="LineComponent/Product/Quantity[@AssignedBy='Supplier'][@WeightType='Net'][@Type='Invoiced']"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteLineUnitCode -->        
	    <xsl:call-template name="lookup">
		    <xsl:with-param name="code" select="LineComponent/Product/Quantity[@AssignedBy='Supplier'][@WeightType='Net'][@Type='Invoiced']/@UnitOfMeasure"/>
			<xsl:with-param name="description" select="'uom'"/>
	    </xsl:call-template>
		<xsl:text>;</xsl:text> 
		
        <!-- it is mismatched with header on 2 pos. here ? one should be removed -->
        <!-- CreditNoteLineExtensionAmount;CreditNoteLineTotalTaxAmount; -->
        <xsl:value-of select="LineComponent/ValueBeforeTax"/><xsl:text>;</xsl:text>        
        <!-- <xsl:value-of select="LineReference[@AssignedBy='Supplier'][@Type='Purchase Order Number']"/><xsl:text>;</xsl:text>  -->
        
        <!--CreditNoteLineTotalTaxAmount-->        
        <xsl:value-of select="LineComponent/Tax"/><xsl:text>;</xsl:text>
        
        <!--CreditNoteLineSubTotalTaxableAmount-->        
        <xsl:value-of select="LineComponent/ValueBeforeTax"/><xsl:text>;</xsl:text>
        
        <!-- CreditNoteLineSubTotalTaxAmount -->        
        <xsl:value-of select="LineComponent/Tax"/>
        <xsl:text>;</xsl:text>
        
        <!-- CreditNoteLineSubTotalTaxCategoryID -->
		<xsl:text>S</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!-- CreditNoteLineSubTotalTaxCategoryPercent -->
        <xsl:value-of select="LineComponent/Tax/@TaxRate"/>
        <xsl:text>;</xsl:text> 
        
        <!--CreditNoteLineSubTotalTaxSchemeID-->
		<xsl:text>VAT</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!--CreditNoteLineItemDescription-->
        <xsl:value-of select="LineComponent/Product/Specification/Description"/>
        <xsl:text>;</xsl:text>
        
        <!--CreditNoteLineSellersItemID-->        
        <xsl:value-of select="LineComponent/Product/Specification/Reference[@AssignedBy='Supplier'][@Type='Identifier']"/>
        <xsl:text>;</xsl:text>        

        <!--CreditNoteLineSellersItemIDScheme-->
        <xsl:value-of select="LineComponent/Product/Specification/Reference[@AssignedBy='Supplier'][@Type='Base Product Code']"/>
        <xsl:text>;</xsl:text>        
        
        <!--CreditNoteLineUnitPrice-->
        <xsl:value-of select="LineComponent/Product/Price/ActualUnitPrice"/>
        <xsl:text>;</xsl:text>
        
        <!--CreditNoteLineBaseQuantityUnitCode-->
        <xsl:call-template name="lookup">
		    <xsl:with-param name="code" select="LineComponent/Product/Price/@UOM"/>
			<xsl:with-param name="description" select="'uom'"/>
        </xsl:call-template>
        <xsl:value-of select="$newLine"/>
       	
	</xsl:template>
	

	<xsl:template match="FinancialDocument[@FinancialDocumentType = 'Tax Invoice']/DocumentLine">
	
        <!-- InvoiceNumber -->
        <xsl:value-of select="../DocumentHeader/DocumentReference[@AssignedBy='Supplier'][@Type='Invoice Number']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceUUID-->
        <xsl:value-of select="../../EnvelopeHeader/EnvelopeTrackingID"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceDate-->
        <xsl:call-template name="formatDate">
			<xsl:with-param name="date"	select="../DocumentHeader/DocumentDate[@AssignedBy='Supplier'][@Type='Invoice Date']"/>
		</xsl:call-template>
		<xsl:text>;</xsl:text>
		 
		<!--InvoiceTaxPointDate--> 
        <xsl:call-template name="formatDate">
			<xsl:with-param name="date"	select="../DocumentHeader/DocumentDate[@AssignedBy='Supplier'][@Type='Tax Point Date']"/>
		</xsl:call-template>
		<xsl:text>;</xsl:text> 
		
		<!--InvoiceCurrencyCode-->
        <xsl:value-of select="../DocumentHeader/@Currency"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceBuyersOrderID-->
        <xsl:value-of select="../DocumentHeader/DocumentReference[@AssignedBy='Buyer'][@Type='Purchase Order Number']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceSellersOrderID-->        
        <xsl:value-of select="../DocumentHeader/DocumentReference[@AssignedBy='Supplier'][@Type='Purchase Order Number']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceOrderDate-->        
        <xsl:if test="../DocumentHeader/DocumentDate[@AssignedBy='Buyer'][@Type='Purchase Order Date'] != ''">
            <xsl:call-template name="formatDate">
                <xsl:with-param name="date"	select="../DocumentHeader/DocumentDate[@AssignedBy='Buyer'][@Type='Purchase Order Date']"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:text>;</xsl:text>
        
        <!-- InvoiceFileReferenceID -->
        <xsl:value-of select="substring ( translate( $ADPTR_MSGID, '-', '' ), 1, 17 )" />
        <xsl:text>;</xsl:text>
        
        <!--InvoiceContractReferenceID-->
        <xsl:value-of select="../DocumentHeader/DocumentReference[@Type='Contract Number']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceSenderCustomerAssignedAccountID-->        
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Reference[@Type='Organisation ID'][@AssignedBy='Third Party']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceSenderPartyID-->        
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Reference[@Type='VAT Registration Number'][@AssignedBy='Government Agency']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceSenderPartyIDScheme-->        
		<xsl:text>GB:VAT</xsl:text>
		<xsl:text>;</xsl:text>		
		
		<!-- Sender info -->
		<!--InvoiceSenderPartyName-->           
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/Name"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceSenderStreetName-->       
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/StreetAddress[1]"/>
        <xsl:text>;</xsl:text> 
        
        <!--InvoiceSenderAdditionalStreetName-->           
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/StreetAddress[2]"/>
        <xsl:text>;</xsl:text> 
        
        <!--InvoiceSenderCityName-->                   
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/CityName"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceSenderPostalZone-->                   
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/PostalCode"/>
        <xsl:text>;</xsl:text>    
        
        <!--InvoiceSenderCountryCode-->           
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Supplier']/Section[@BusinessTransactionRole='Invoice From Party']/Location[@Type='Section Address']/Address/CountryCode"/>
        <xsl:text>;</xsl:text>                

		<!-- Receiver info -->
		<!--InvoiceReceiverSupplierAssignedAccountID-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Reference[@Type='Organisation ID'][@AssignedBy='Third Party']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceReceiverPartyID-->        
        <xsl:call-template name="lookup">
            <xsl:with-param name="code">
                <xsl:call-template name="removeLeadingZeros">
                    <xsl:with-param name="removeLeadingZeros" select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Reference[@Type='Organisation ID'][@AssignedBy='Buyer']" />
                </xsl:call-template>
            </xsl:with-param>
            <xsl:with-param name="description" select="'-----------'" /> 
        </xsl:call-template>         
        <xsl:text>;</xsl:text>        
        
        <!--InvoiceReceiverPartyIDScheme-->
        <xsl:choose>
            <xsl:when test="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location/Address/CountryCode = 'IE'">
                <xsl:text>IE:VAT</xsl:text>
            </xsl:when>
            <xsl:otherwise>GB:VAT</xsl:otherwise>
        </xsl:choose>
		<xsl:text>;</xsl:text>
		
		<!--InvoiceReceiverPartyName-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/Name"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceReceiverStreetName-->        
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/StreetAddress[1]"/>
        <xsl:text>;</xsl:text>            
        
        <!--InvoiceReceiverAdditionalStreetName-->
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/StreetAddress[2]"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceReceiverCityName-->                    
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/CityName"/>
        <xsl:text>;</xsl:text>       
        
        <!--InvoiceReceiverPostalZone-->            
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/PostalCode"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceReceiverCountryCode-->               
        <xsl:value-of select="../DocumentHeader/Organisation[@Type='Buyer']/Section[@BusinessTransactionRole='Bill To Party']/Location[@Type='Section Address']/Address/CountryCode"/>
        <xsl:text>;</xsl:text>       
                
		<!-- Delivery Info -->
		<!--InvoiceDeliveryLocationID-->	
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Reference"/>
        <xsl:text>;</xsl:text>  
        
        <!--InvoiceDeliveryLocationDescription-->      
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/Name"/>
        <xsl:text>;</xsl:text> 
        
        <!--InvoiceDeliveryStreetName-->      
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/StreetAddress[1]"/>
        <xsl:text>;</xsl:text>        

        <!--InvoiceDeliveryAdditionalStreetName-->
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/StreetAddress[2]"/>-<xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/StreetAddress[3]"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceDeliveryCityName-->        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/CityName"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceDeliveryPostalZone-->        
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/PostalCode"/>
        <xsl:text>;</xsl:text>  
        
        <!--InvoiceDeliveryCountryCode-->      
        <xsl:value-of select="MovementDetails/Location[@Type='Delivery Point']/Address/CountryCode"/>
        <xsl:text>;</xsl:text>  
              
        <!--InvoicePaymentMeansCode-->
        <xsl:value-of select="../DocumentHeader/TermsAndConditions/PaymentTerms/PaymentMethod"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoicePaymentTermsNote-->        
        <xsl:value-of select="../DocumentHeader/TermsAndConditions/PaymentTerms/Reference"/>
        <xsl:text>;</xsl:text>   
             
		<!-- Invoice totals -->
		<!--InvoiceTotalTaxAmount-->
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalTax"/>
        <xsl:text>;</xsl:text>  
        
        <!--InvoiceSubTotalTaxableAmount-->      
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueBeforeTax"/>
        <xsl:text>;</xsl:text> 
        
        <!--InvoiceSubTotalTaxAmount-->       
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalTax"/>
        <xsl:text>;</xsl:text>     
        
        <!--InvoiceSubTotalTaxCategoryID-->   
		<xsl:text>S</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!--InvoiceSubTotalTaxCategoryPercent-->
        <xsl:value-of select="../DocumentTrailer/TaxTrailer/@TaxRate"/>
        <xsl:text>;</xsl:text>   
        
        <!--InvoiceSubTotalTaxSchemeID-->     
		<xsl:text>VAT</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!--InvoiceTotalLineAmount-->
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueBeforeTax"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceTaxTotal-->        
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalTax"/>
        <xsl:text>;</xsl:text>  
         
        <!--InvoiceTotal-->      
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueAfterTax"/>
        <xsl:text>;</xsl:text>     
        
        <!--InvoicePayableAmount-->   
        <xsl:value-of select="../DocumentTrailer/Totals[@TotalType='Control']/TotalValueAfterTax"/>
        <xsl:text>;</xsl:text>
        
           
        <!-- Documentline info -->
        <!--InvoiceLineID-->
        <xsl:value-of select="@LineNumber"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceLineQuantity-->        
        <xsl:value-of select="LineComponent/Product/Quantity[@AssignedBy='Supplier'][@WeightType='Net'][@Type='Invoiced']"/>
        <xsl:text>;</xsl:text> 
        
        <!--InvoiceLineUnitCode-->       
        <xsl:call-template name="lookup">
            <xsl:with-param name="code" select="LineComponent/Product/Quantity[@AssignedBy='Supplier'][@WeightType='Net'][@Type='Invoiced']/@UnitOfMeasure"/>
			<xsl:with-param name="description" select="'uom'"/>
		</xsl:call-template>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceLineExtensionAmount-->
        <xsl:value-of select="LineComponent/ValueBeforeTax"/>
        <xsl:text>;</xsl:text>      
          
        <!--xsl:value-of select="LineReference[@AssignedBy='Supplier'][@Type='Purchase Order Number']"/><xsl:text>;</xsl:text-->   
        
        <!--InvoiceLineTotalTaxAmount-->     
        <xsl:value-of select="LineComponent/Tax"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceLineSubTotalTaxableAmount-->        
        <xsl:value-of select="LineComponent/ValueBeforeTax"/>
        <xsl:text>;</xsl:text>        
        
        <!--InvoiceLineSubTotalTaxAmount-->
        <xsl:value-of select="LineComponent/Tax"/>
        <xsl:text>;</xsl:text>  
        
        <!--InvoiceLineSubTotalTaxCategoryID-->      
		<xsl:text>S</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!--InvoiceLineSubTotalTaxCategoryPercent-->
        <xsl:value-of select="LineComponent/Tax/@TaxRate"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceLineSubTotalTaxSchemeID-->        
		<xsl:text>VAT</xsl:text>
		<xsl:text>;</xsl:text>
		
		<!--InvoiceLineItemDescription-->
        <xsl:value-of select="LineComponent/Product/Specification/Description"/>
        <xsl:text>;</xsl:text> 
        
        <!--InvoiceLineSellersItemID-->       
        <xsl:value-of select="LineComponent/Product/Specification/Reference[@AssignedBy='Supplier'][@Type='Identifier']"/>
        <xsl:text>;</xsl:text>    
        
        <!--InvoiceLineSellersItemIDScheme-->    
        <xsl:value-of select="LineComponent/Product/Specification/Reference[@AssignedBy='Supplier'][@Type='Base Product Code']"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceLineUnitPrice-->        
        <xsl:value-of select="LineComponent/Product/Price/ActualUnitPrice"/>
        <xsl:text>;</xsl:text>
        
        <!--InvoiceLineBaseQuantityUnitCode-->        
        <xsl:call-template name="lookup">
            <xsl:with-param name="code" select="LineComponent/Product/Price/@UOM"/>
            <xsl:with-param name="description" select="'uom'"/>
		</xsl:call-template>
		<xsl:value-of select="$newLine"/>
       	
	</xsl:template>

	<!-- 
		utility templates 
	-->		

    <!--
        Name: 		removeLeadingZeros  
        Parameters: removeLeadingZeros - string to remove leading "0", like "00000000123" -> "123"
        Output:		string without leading "0"
        Comment:	taken from F4F template
    -->
    <xsl:template name="removeLeadingZeros">
        <xsl:param name="removeLeadingZeros"/>
        <xsl:choose>
            <xsl:when test="starts-with($removeLeadingZeros,'0')">
                <xsl:call-template name="removeLeadingZeros">
                    <xsl:with-param name="removeLeadingZeros">
                        <xsl:value-of select="substring-after($removeLeadingZeros,'0' )"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$removeLeadingZeros"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
		
	<!-- catch all do nothing template, just to move over everything -->
	<xsl:template match="@*|node()">
		<xsl:apply-templates select="@*|node()"/>
	</xsl:template>		

 </xsl:stylesheet>
